<?php

namespace App\Controller;

use App\Model\ConnectionLog;
use App\Model\User;

use Slim\Http\Request;
use Slim\Http\Response;

class AuthController extends Controller
{

    /**
     * Auth an user
     *
     * @param Request $request
     * @param Response $response
     *
     * @return
     *      Response, or let them log-in.
     */

    public function authenticate(Request $request, Response $response) {

        $params = $request->getParams();

        $username = isset($params['username']) ? $params['username'] : null;
        $password = isset($params['password']) ? $params['password'] : null;
        $clientToken = isset($params['clientToken']) ? $params['clientToken'] : null;

        $user = User::where("name", $username)->first();
        
        if (!$user)
            return error(2, $response);

        if(!password_verify($password, $user->password))
            return error(2, $response);

        if(is_null($user->uuid))
            $user->uuid = generateUUID();

        $connectionLog = new ConnectionLog();
        $connectionLog->username = $user->name;
        $connectionLog->type = "Launcher";
        $connectionLog->ip = $_SERVER['REMOTE_ADDR'];
        $connectionLog->save();

        $accessToken = md5(uniqid(rand(), true));
        if (is_null($clientToken))
            $clientToken = md5(uniqid(rand(), true));
        $user->access_token = $accessToken;
        $user->client_token = $clientToken;
        $user->save();

        return $response->withJson([
            'accessToken' => $accessToken,
            'clientToken' => $clientToken,
            'availableProfiles' => [
                [
                    'id' => $user->uuid,
                    'name' => $user->name
                ]
            ],
            'selectedProfile' => [
                'id' => $user->uuid,
                'name' => $user->name
            ]
        ]);
    }
}