<?php

namespace App\Controller;

use Interop\Container\ContainerInterface;

class Controller {
    protected $container;

    public function __construct(ContainerInterface $ci) {
        $this->container = $ci;
    }

}