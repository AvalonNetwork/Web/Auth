<?php

namespace App\Controller;

use \Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;
use Slim\Http\Request;
use Slim\Http\Response;

class DatabaseController extends Controller
{

    public function up(Request $request, Response $response) {

        Capsule::schema('auth')->create('logs', function(Blueprint $table){
            $table->increments('id');
            $table->string('username');
            $table->string('type', 8)->default('Launcher');
            $table->ipAddress('ip');
            $table->timestamp('time')->useCurrent = true;
        });

        Capsule::schema('auth')->create('sessions', function(Blueprint $table){
            $table->increments('id');
            $table->string('username');
            $table->string('uuid', 36);
            $table->ipAddress('server');
            $table->timestamp('time')->useCurrent = true;
        });

    }

    public function fill(Request $request, Response $response) {

    }

    public function down(Request $request, Response $response) {
        Capsule::schema('auth')->dropIfExists('logs');
        Capsule::schema('auth')->dropIfExists('sessions');
    }

}