<?php

namespace App\Controller;

use App\Model\User;
use Slim\Http\Request;
use Slim\Http\Response;

class ProfileController extends Controller
{

    /**
     * Refresh OpenAuth Profile
     *
     * @param Request $request
     * @param Response $response
     * @return null|Response
     */

    public function refresh(Request $request, Response $response) {

        $params = $request->getParams();

        $clientToken = !empty($params['clientToken']) ? $params['clientToken'] : null;
        $accessToken = !empty($params['accessToken']) ? $params['accessToken'] : null;

        $user = User::where("accessToken", $accessToken)->first();

        if (!$user)
            return error(3, $response);

        if ($user->clientToken != $clientToken)
            return error(2, $response);

        $user->accessToken = md5(uniqid(rand(), true));
        $user->save();

        return $response->withJson([
            'accessToken' => $user->accessToken,
            'clientToken' => $clientToken
        ]);
    }

    /**
     * Validate a OpenAuth Profile
     *
     * @param Request $request
     * @param Response $response
     * @return null
     */

    public function validate(Request $request, Response $response){

        $params = $request->getParams();

        $accessToken = !empty($params['accessToken']) ? $params['accessToken'] : null;

        if(is_null($accessToken))
            return error(3, $response);

        if(!User::where("access_token", $accessToken)->first())
            return error(3, $response);
    }

    /**
     * Invalid OpenAuth Profile
     *
     * @param Request $request
     * @param Response $response
     * @return array|Response|string
     */

    public function invalidate(Request $request, Response $response){

        $params = $request->getParams();

        $accessToken = !empty($params['accessToken']) ? $params['accessToken'] : null;
        $clientToken = !empty($params['clientToken']) ? $params['clientToken'] : null;

        if(empty($accessToken) || empty($clientToken))
            return error(3, $response);

        $user = User::where("access_token", $accessToken)->first();

        if(!$user)
            return error(3, $response);

        if ($clientToken != $user->client_token)
            return error(3, $response);

        $user->access_token = null;
        $user->save();
    }

    /**
     * Sent OpenAuth Profile
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     *
     * @return mixed
     */

    public function profile(Request $request, Response $response, $args){
        if(!isset($args['uuid']))
            return error(3, $response);

        $uuid = $args['uuid'];
        $user = User::where('uuid', $uuid)->orderBy('id', 'desc')->first();

        if (!$user)
            return error(3, $response);

        return $response->withJson([
            'id' => $uuid,
            'name' => $user->name,
            'properties' => array(
                'name' => 'textures',
                'value' => base64_encode(json_encode([
                    'timestamp' => time() * 1000,
                    'profileId' => $uuid,
                    'profileName' => $user->name,
                    'isPublic' => true,
                    'textures' => [
                        'SKIN' => 'http://skins.avalon-network.net/skins/' . $user->name . '.png',
                        'CAPE' => 'http://skins.avalon-network.net/capes/' . $user->name . '.png'
                    ]
                ]))
            )
        ]);
    }

}