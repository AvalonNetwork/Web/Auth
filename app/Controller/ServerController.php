<?php

namespace App\Controller;

use App\Model\Session;

use App\Model\User;
use Slim\Http\Request;
use Slim\Http\Response;

class ServerController extends Controller
{

    public function join(Request $request, Response $response){

        $params = $request->getParams();

        $accessToken = !empty($params['accessToken']) ? $params['accessToken'] : null;
        $uuid = !empty($params['selectedProfile']) ? $params['selectedProfile'] : null;
        $serverId = !empty($params['serverId']) ? $params['serverId'] : null;

        if(empty($accessToken) || empty($uuid) || empty($serverId))
            return error(3, $response);

        $user = User::where("access_token", $accessToken)->first();

        if(!$user)
            return error(3, $response);

        Session::where('uuid', $uuid)->delete();

        $cache = new Session();
        $cache->username = $user->name;
        $cache->uuid = $uuid;
        $cache->server = $serverId;
        $cache->save();

        return $response->withJson([
            'error' => null,
            'errorMessage' => null,
            'cause' => null
        ]);
    }


    public function hasJoined(Request $request, Response $response) {

        $params = $request->getQueryParams();

        $username = !empty($params['username']) ? $params['username'] : null;
        $serverId = !empty($params['serverId']) ? $params['serverId'] : null;

        if(empty($username) || empty($serverId))
            return error(3, $response);

        $session = Session::where('username', $username)->orderBy('id', 'desc')->first();

        if (!$session)
            return error(7, $response);

        $uuid = $session->uuid;
        Session::where('username', $username)->delete();

        return $response->withJson([
            'id' => $uuid,
            'properties' => [
                [
                    'name' => "textures",
                    'value' => "",
                    'signature' => ""
                ]
            ]
        ]);
    }
}