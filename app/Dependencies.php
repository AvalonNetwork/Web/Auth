<?php

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
foreach ($container->get('settings')['database'] as $name => $infos)
    $capsule->addConnection($infos, $name);
    $capsule->bootEloquent();
    $capsule->setAsGlobal();