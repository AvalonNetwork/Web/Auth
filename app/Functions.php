<?php

function error($error, Response $response, $reason = '')
{
    $errors = [
        [
            "error" => "Method Not Allowed",
            "errorMessage" => "The method specified in the request is not allowed for the resource identified by the request URI"
        ],
        [
            "error" => "Not Found",
            "errorMessage" => "The server has not found anything matching the request URI"
        ],
        [
            "error" => "ForbiddenOperationException",
            "errorMessage" => "Identifiants invalides."
        ],
        [
            "error" => "ForbiddenOperationException",
            "errorMessage" => "Invalid token."
        ],
        [
            "error" => "IllegalArgumentException",
            "errorMessage" => "Access token already has a profile assigned."
        ],
        [
            "error" => "Unsupported Media Type",
            "errorMessage" => "The server is refusing to service the request because the entity of the request is in a format not supported by the requested resource for the requested method"
        ],
        [
            "error" => "ForbiddenOperationException",
            "errorMessage" => "Cette IP n'est pas autorisé par ObsiGuard."
        ],
        [
            "error" => "ForbiddenOperationException",
            "errorMessage" => "Vous êtes bannis: $reason."
        ]
    ];

    // Creating an array by the error and the error message
    if (!array_key_exists($error, $errors))
        $response->withStatus(500)
            ->withJson(["error" => "Unknown error"]);

    $status = 500;
    if($error == 1)
        $status = 404;

    // Returning the JSON
    return $response->withStatus($status)->withJson($errors[$error]);
}

/**
 * Generate UUID
 * @return string
 */

function generateUUID()
{
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),

        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,

        // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}