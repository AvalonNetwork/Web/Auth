<?php

namespace App\Model;

class ConnectionLog extends Model
{

    public $timestamps = false;

    protected $connection = 'auth';
    protected $table = "logs";

}