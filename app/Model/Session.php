<?php

namespace App\Model;

class Session extends Model
{

    public $timestamps = false;

    protected $connection = 'auth';
    protected $table = "sessions";
}