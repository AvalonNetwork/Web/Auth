<?php

use \App\Controller\DatabaseController;

use \App\Controller\AuthController;
use \App\Controller\ProfileController;
use \App\Controller\ServerController;

$app->get('/up', DatabaseController::class . ':up')
    ->setName("db.up");

/**
 *  Authentification Routes
 */

// auth & join en post

$app->post('/authenticate', AuthController::class . ':authenticate')
    ->setName("auth.authenticate");

$app->post('/refresh', ProfileController::class . ':refresh')
    ->setName("auth.refresh");
$app->post('/validate', ProfileController::class . ':validate')
    ->setName("auth.validate");
$app->post('/signout', ProfileController::class . ':signout')
    ->setName("auth.signout");
$app->post('/invalidate', ProfileController::class . ':invalidate')
    ->setName("auth.invalidate");

$app->post('/join', ServerController::class . ':join')
    ->setName("auth.join");
$app->get('/hasJoined', ServerController::class . ':hasJoined')
    ->setName("auth.hasJoined");

$app->get('/profile/{uuid}', ProfileController::class . ':profile')
    ->setName("auth.profile");