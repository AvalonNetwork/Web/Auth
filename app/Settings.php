<?php

return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
        'installed' => true,

        // Database
        'database' => [
            'auth' => [
                'driver'    => 'mysql',
                'host'      => 'localhost',
                'database'  => 'auth',
                'username'  => 'root',
                'password'  => '',
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ],
            'web' => [
                'driver'    => 'mysql',
                'host'      => 'localhost',
                'database'  => 'web',
                'username'  => 'root',
                'password'  => '',
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ],
            'factions' => [
                'driver'    => 'mysql',
                'host'      => 'localhost',
                'database'  => 'factions',
                'username'  => 'root',
                'password'  => '',
                'charset'   => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix'    => '',
            ],
        ]
    ]
];
