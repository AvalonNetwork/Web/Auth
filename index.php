<?php

require 'vendor/autoload.php';

session_start();

$settings = require __DIR__ . '/app/Settings.php';
$app = new \Slim\App($settings);

require __DIR__ . '/app/Functions.php';
require __DIR__ . '/app/Dependencies.php';
require __DIR__ . '/app/Routes.php';

$app->run();