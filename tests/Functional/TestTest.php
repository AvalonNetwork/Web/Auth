<?php
/**
 * Created by PhpStorm.
 * User: Alexis
 * Date: 26/05/2018
 * Time: 14:13
 */

namespace Tests;

use Tests\Functional\BaseTestCase;

class HomepageTest extends BaseTestCase
{
    public function testGetHomepageWithGreeting()
    {
        $response = $this->runApp('GET', '/name');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('Hello, name', (string)$response->getBody());
    }


    /**
     * Test that the index route won't accept a post request
     */
    public function testPostHomepageNotAllowed()
    {
        $response = $this->runApp('POST', '/e', ['test']);

        $this->assertEquals(405, $response->getStatusCode());
        $this->assertContains('Method not allowed', (string)$response->getBody());
    }
}